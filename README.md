[![pipeline status](https://gitlab.eclipse.org/eclipse/xfsc/tsa/task/badges/main/pipeline.svg)](https://gitlab.eclipse.org/eclipse/xfsc/tsa/task/-/commits/main)
[![coverage report](https://gitlab.eclipse.org/eclipse/xfsc/tsa/task/badges/main/coverage.svg)](https://gitlab.eclipse.org/eclipse/xfsc/tsa/task/-/commits/main)

# Task Service

The task service provides an HTTP interface for executing asynchronous (HTTP) tasks and task lists.

It is developed using the [Goa v3](https://goa.design/) framework.

While the service is up and running, you can see a live Swagger API
description at `servicehost:serviceport/swagger-ui`. In the local docker-compose
environment, the Swagger URL is available at http://localhost:8082/swagger-ui/


### High-level Overview

```mermaid  
flowchart LR  
	A([client]) -- HTTP --> B[Task API] 
	subgraph task 
		B --> C[(tasks DB)] 
		C --> D[Executor] 
	end 
	D --> E[Policy]
	D --> F[Cache]
```

Tasks are created by clients making HTTP requests. The newly created tasks are
stored in a persistent database which is used like Queue. An executor component
is retrieving tasks from the Queue for execution. Clients receive an 
immediate response with the `taskID` for the created task and can later query
the state of task and retrieve its result either by directly querying the Cache
service, or by querying the task HTTP interface for task results.

### More information
* [Tasks](docs/task.md)
* [Task lists](docs/task-list.md)
* [Queue](docs/queue.md)





### Tests and Linters

To execute the units tests for the service go to the root project directory and run:
```go
go test -race ./...
```

To run the linters go to the root project directory and run:
```go
golangci-lint run
```

## Dependencies

The project uses Go modules for managing dependencies, and we commit the `vendor` directory.
When you add/change dependencies, be sure to clean and update the `vendor` directory before
submitting your Merge Request for review.
```shell
go mod tidy
go mod vendor
```

[Dependencies](go.mod)

## License
<hr/>

[Apache 2.0 license](LICENSE)
